﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDb_Trial
{
    public class Train
    {

        public ObjectId Id { get; set; }

        public string Name { get; set; }

        public List<Station> GoesTo { get; set; }

        public int Capacity { get; set; }

        public int PassengerCount { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, Cap:{1}", Name, Capacity);
        }

        public string GoesToStations
        {
            get
            {
                if (GoesTo == null || GoesTo.Count == 0) return "not set";
                else
                {
                    StringBuilder sb = new StringBuilder();
                    GoesTo.ForEach(x => sb.AppendFormat("{0},", x.Name));
                    return sb.ToString();
                }
            }
        }

    }

    public class Station
    {
        public ObjectId Id { get; set; }

        public string Name { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }
   }

}
