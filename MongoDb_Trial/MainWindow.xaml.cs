﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MongoDb_Trial
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            VM = new MainWindowViewModel();
            if (VM.ActiveTrain == null) VM.ActiveTrain = new Train();
        }

        public MainWindowViewModel VM
        {
            get
            {
                return this.DataContext as MainWindowViewModel;
            }
            private set
            {
                this.DataContext = value;
            }
        }

        private void btnAddTrain_Click(object sender, RoutedEventArgs e)
        {

            //Train t = new Train() { Name = tbName.Text, Capacity = Int32.Parse(tbCapacity.Text) };
            VM.Save();
        }
    }
}
