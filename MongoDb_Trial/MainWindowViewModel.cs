﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace MongoDb_Trial
{
    public class MainWindowViewModel : Bindable
    {

        string connString = "mongodb://10.1.1.136";
        MongoClient client = null;//thread-safe, everyone's happy
        MongoServer server = null;
        MongoDatabase db = null;

        public MainWindowViewModel()
        {
            MongoClientSettings settings = new MongoClientSettings();
            client = new MongoClient(connString);
            server = client.GetServer();
            Init();
        }

        public List<Train> Trains
        {
            get
            {
                return db.GetCollection("Trains").FindAllAs<Train>().ToList();
            }
        }

        public List<Station> Stations
        {
            get
            {
                return db.GetCollection("Stations").FindAllAs<Station>().ToList();
            }
        }

        private void Init()
        {
            if (!server.DatabaseExists("Railway"))
            {
                throw new Exception("Create database (mongo shell, use Railway)");
            }
            db = server.GetDatabase("Railway");

            if (!db.CollectionExists("Trains")) db.CreateCollection("Trains");
            //AddEmbeddedTrains();
            OnPropertyChanged("Trains");
        }

        private void AddEmbeddedTrains()
        {
            Station a = new Station() { Name = "a", Latitude = 1, Longitude = 1 };
            Station b = new Station() { Name = "b", Latitude = 2, Longitude = 3 };
            Station c = new Station() { Name = "c", Latitude = 4, Longitude = 3 };
            Station d = new Station() { Name = "d", Latitude = 4, Longitude = 1 };

            List<Train> trains = new List<Train>()
            {
                new Train() { Name="Thomas", Capacity=10, GoesTo=new List<Station>() {a, b, c}},
                new Train() { Name="Philip", Capacity=15, GoesTo=new List<Station>() {a, b, d}},
                new Train() {Name="Patrick", Capacity=20, GoesTo=new List<Station>() {b, c, d}}
            };

            trains.ForEach(t => AddTrain(t));
        }

        public Train ActiveTrain
        {
            get
            {
               return Get<Train>();
            }
            set
            {
                Set<Train>(value);
            }

        }

        public void AddTrain(Train t)
        {
            db.GetCollection("Trains").Insert(t);
            OnPropertyChanged("Trains");
        }

        public void Save()
        {
            db.GetCollection("Trains").
            db.GetCollection("Trains").Save(ActiveTrain);
        }

        private void UpdateTrain(Train t)
        {
            var q = Query<Train>.EQ(x => x.Id, t.Id);
            var update = Update<Train>.Set(x => x.Capacity, t.Capacity);
            db.GetCollection("Trains").Update(q, update);
        }

    }

}
